const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI" //can be any string

//JWT is a way of securely passing information from the server to the front end
//Information is kept secure through the use of the secret
//Only the API that knows the secret can access the information

//Creation of the token (Analogy: Pack the gift, and seal with the secret)
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {}) //sign is a built-in jsonwebtoken function
}

//Verification of the token (Analogy: receive the gift and verify that gift was not tampered with)
module.exports.verify = (req, res, next) => {
	//get our JSONWebToken, which is found in the authorization header
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token)
		//slice removes the unnecessary "Bearer:" part of our token
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"})
			}else{
				next();
			}
		})
	}else{ //if the token is not present
		return res.send({auth: "failed"})
	}
}

//Decoding of the token (analogy: open the gift and get the contents)
module.exports.decode = (token) => {
	//check if token is present
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}else{
				return jwt.decode(token, {complete: true}).payload
				//payload is the data from the token when we created the access token
			}
		})
	}else{
		return null;
	}
}
