//Import dependencies
const express = require("express")
const mongoose = require("mongoose")

//Import routes 
const userRoutes = require("./routes/user") //allows us to use the content of our user routes file
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")
const cors = require("cors")

//add database connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.qgo12.mongodb.net/eCommerceCapstone?retryWrites=true&w=majority", {
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

//confirm ATLAS connection in console
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

//server setup
const app = express()
app.use(cors())

app.use(express.json())//allows data to read and send json data
app.use(express.urlencoded({
	extended: true
})); //allows your server to read data from forms
// app.use(function(req, res, next) {
// res.header("Access-Control-Allow-Origin", "*");
// res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
// next();
// });

//add routes 
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)

const port = 4000 

//listen to the server
app.listen(process.env.PORT || port, () => {
	console.log(`Server running at port ${port}`)
})
//process.env.PORT is the port that is assigned by your hosting service once your server is deployed on the internet. Originally, we can use any port that we want while developing (such as 4000), however once our server is live, it is up tp the service that is hosting your server to give it a port, and that is what process.env.PORT is and why we use the OR operator (so that we can still use 4000 while developing)