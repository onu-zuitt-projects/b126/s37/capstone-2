const express = require ('express')
const router = express.Router()
const productController = require("../controllers/product")
const auth = require("../auth")

//route to create a new product
//auth.verify below is what is called "Middleware" 
router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin===false){
		res.send({auth: "failed"})
	}else{
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
	}

	//console.log(req.headers.authorization) - used to grant authorization to an admin by giving tokens for creating new courses
	//console.log(req.body)

	//Create your own way to make sure that ONLY admins can create a new course.
	//If a non-admin tries, they simply receive an error message saying (auth: failed)
	//Make sure to test all cases in Postman

	//auth.decode turns out token into a decoded JavaScript object that we can use the dot notation on to access its properties (such as isAdmin)

// 	if(auth.decode(req.headers.authorization).isAdmin){
// 		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
// 	}else{
// 		res.send({auth: "failed"})
// 	}

})

//route for retrieving all products
router.get("/", (req, res) => {
	console.log(req)
	productController.getProducts().then(resultFromController => res.send(
		resultFromController))
})

//route for retrieving all ACTIVE products
router.get("/", (req, res) => {
	console.log(req)
	productController.getActiveProducts().then(resultFromController => res.send(
		resultFromController))
})

//route for retrieving a specific product
router.get("/:productId", (req, res) => {
	//console.log(req.params)
	//productId is called a "wildcard" and determines the params object's key
	productController.getProduct(req.params).then(resultFromController => res.send(
		resultFromController))
})

//update product information
router.put("/:productId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}

})

//checkout product 
router.post("/:checkout", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization)){
		productController.Product(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}

})



router.delete("/:productId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}
})

//req.params: 
//Anything you need to pass in the URL (Often IDS)

//req.headers.authorization:
//Token

//req.body:
//Everything else

//These 3 (above) are all used in updating an information (e.g. a user)

module.exports = router;